import React from 'react'
import { Bar } from 'react-chartjs-2'
import axios from 'axios'
import styled from 'styled-components'

const Header = styled.div`
  text-align: center;
  padding: 50px 100px 50px 100px;

  .myHeader {
    font-size: 42px;
    color: #0D2E41;
  }
`
const Subheader = styled.div`
  text-align: center;
  font-weight: 300;
  font-size: 26px;
`

class Statistic extends React.Component {
  constructor(props) {
    super(props)
    this.state = { statistic: [] }
  }
  
  componentDidMount() {
    axios.get('/api/v1/statistic.json')
    .then( resp => this.setState({ statistic: resp.data }))
    .catch( resp => console.log(resp) )
  }
  

  returnCountries() {
    const { statistic } = this.state

    return statistic.map( statistic => (statistic.country))
  }

  returnCounts() {
    const { statistic } = this.state

    return statistic.map( statistic => (statistic.count))
  }
  
  render() {
    return (
      <div>
        <Header>
          <h1 className="myHeader">Airlines</h1>
          <Subheader>Count of airlines per each country.</Subheader>
        </Header>
  
        <div>
          <Bar
            data={{
              labels: this.returnCountries(),
              datasets: [
                {
                  label: 'Count of airlines per country',
                  data: this.returnCounts(),
                  backgroundColor: [
                    `rgba(${Math.random() * 256}, ${Math.random() * 256}, ${Math.random() * 256}, 0.3)`,
                    `rgba(${Math.random() * 256}, ${Math.random() * 256}, ${Math.random() * 256}, 0.3)`,
                    `rgba(${Math.random() * 256}, ${Math.random() * 256}, ${Math.random() * 256}, 0.3)`,
                    `rgba(${Math.random() * 256}, ${Math.random() * 256}, ${Math.random() * 256}, 0.3)`,
                    `rgba(${Math.random() * 256}, ${Math.random() * 256}, ${Math.random() * 256}, 0.3)`,
                    `rgba(${Math.random() * 256}, ${Math.random() * 256}, ${Math.random() * 256}, 0.3)`,
                    `rgba(${Math.random() * 256}, ${Math.random() * 256}, ${Math.random() * 256}, 0.3)`,
                    `rgba(${Math.random() * 256}, ${Math.random() * 256}, ${Math.random() * 256}, 0.3)`,
                    `rgba(${Math.random() * 256}, ${Math.random() * 256}, ${Math.random() * 256}, 0.3)`,
                    `rgba(${Math.random() * 256}, ${Math.random() * 256}, ${Math.random() * 256}, 0.3)`,
                  ],
                },
              ]
            }}
            height={400}
            width={600}
            options={{
              maintainAspectRatio: false,
              scales: {
                yAxes: [
                  {
                    ticks: {
                      beginAtZero: true,
                    }
                  }
                ]
              }
            }}
          />
        </div>
      </div>
    )
  }
}

export default Statistic
