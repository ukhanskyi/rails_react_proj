import React, { useState, useEffect, Fragment } from 'react'
import { BrowserRouter as Router, Link } from 'react-router-dom'
import axios from 'axios'
import Airline from './Airline'
import styled from 'styled-components'

const Home = styled.div`
  text-align: center;
  max-width:1200px;
  margin-left: auto;
  margin-right: auto;
`
const Header = styled.div`
  padding: 100px 100px 10px 100px;

  h1 {
    font-size: 42px;
  }
`
const Subheader = styled.div`
  font-weight: 300;
  font-size: 26px;
`
const Grid = styled.div`
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  grid-gap: 20px;
  width: 100%;
  padding: 20px;
`
const LinkWrapper = styled.div`
  margin: 30px 0 20px 0;
  height: 70px;

  a {
    font-size: 26px;
    font-weight: bold;
    color: whitesmoke;
    background: #0D2E41;
    border-radius: 4px;
    padding: 10px 50px;
    border: 1px solid #000;
    width: 100%;
    text-decoration: none;
  }
`

const Airlines = () => {
  const [airlines, setAirlines] = useState([])

  useEffect(()=> {
    axios.get('/api/v1/airlines.json')
    .then( resp => setAirlines(resp.data.data))
    .catch( resp => console.log(resp) )
  }, [airlines.length])

  const grid = airlines.map( item => {
    return (
      <Airline
        key={item.attributes.name}
        attributes={item.attributes}
      />
    )
  })

  return (
    <Home>
      <Header>
        <h1>OpenFlights</h1>
        <Subheader>Honest, unbiased airline reviews.</Subheader>
      </Header>
      <Grid>
        {grid}
      </Grid>
      <LinkWrapper>
        <Link to={'/statistic'}>View Statistic</Link>
      </LinkWrapper>
      <LinkWrapper>
        <Link to={'/exchange_rate'}>Open Current Exchange Rates</Link>
      </LinkWrapper>

    </Home>
  )
}

export default Airlines
