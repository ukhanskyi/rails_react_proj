import React from 'react'
import { Route, Switch } from 'react-router-dom'
import Airlines from './Airlines/Airlines'
import Airline from './Airline/Airline'
import Statistic from './Airlines/Statistic/Statistic'
import ExchangeRates from './ExchangeRates/ExchangeRates'

const App = () => {
  return (
    <Switch>
      <Route exact path="/" component={Airlines}/>
      <Route exact path="/airlines/:slug" component={Airline}/>
      <Route exact path="/statistic" component={Statistic}/>
      <Route exact path="/exchange_rate" component={ExchangeRates}/>
    </Switch>)
}

export default App