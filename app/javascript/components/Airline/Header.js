import React from 'react'
import styled from 'styled-components'
import Rating from '../Rating/Rating'

const Wrapper = styled.div`
  padding: 50px 100px 50px 0px;
  font-size:30px;
  img {
    margin-right: 10px;
    height: 150px;
    width: 150px;
    border: 1px solid rgba(0,0,0,0.1);
    border-radius: 100%;
    margin-bottom: -8px;
  }
`

const TotalReviews = styled.div`
  font-size: 18px;
  padding: 10px 0;

`

const TotalOutOf = styled.div`
  padding-top: 12px;
  font-size: 18px;
  font-weight: bold;
`

const Header = (props) => {
  const {name, image_url, avg_score} = props.attributes
  const total = props.reviews.length

  return (
    <Wrapper>
      <h1><img src={image_url.url} height="150" width="150" alt={name}/>{name}</h1>
      <div>
        <TotalReviews>{total} user reviews</TotalReviews>
        <Rating score={avg_score}/>
        <TotalOutOf>{avg_score.toFixed(1)} out of 5 stars</TotalOutOf>
      </div>
    </Wrapper>
  )
}

export default Header