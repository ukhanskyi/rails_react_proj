import React, { useState, useEffect } from 'react'
import axios from 'axios'
import './ExchangeRates.scss'

const ExchangeRates = () => {
  const [exchange_rates, setExchangeRates] = useState([])

  useEffect(()=> {
    axios.get('/api/v1/exchange_rate.json')
    .then( resp => setExchangeRates(resp.data.data))
    .catch( resp => console.log(resp) )
  }, [exchange_rates.length])

  const renderTableData = exchange_rates.map( exchange_rate => {
    return (
      <tr key={exchange_rate.attributes.ccy}>
          <td>{exchange_rate.attributes.ccy}</td>
          <td>{exchange_rate.attributes.base_ccy}</td>
          <td>{exchange_rate.attributes.buy} {exchange_rate.attributes.base_ccy}</td>
          <td>{exchange_rate.attributes.sale} {exchange_rate.attributes.base_ccy}</td>
      </tr>
    )
  })

  return (
    <div>
      <div>
        <h1 className="myHeader">Current Exchange Rate</h1>
        <p className="subheader">Get the most common currency value.</p>
      </div>

      <table>
        <thead>
          <tr>
            <th>Currency code</th>
            <th>National currency code</th>
            <th>Purchase rate</th>
            <th>Sales rate</th>
          </tr>
        </thead>
        <tbody>
          {renderTableData}
        </tbody>
      </table>
    </div>
  )
}

export default ExchangeRates
