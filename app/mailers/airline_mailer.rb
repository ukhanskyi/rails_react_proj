# frozen_string_literal: true

# Mailer class which send information about creating new airline
class AirlineMailer < ApplicationMailer
  def send_email_about_new_airline(airline)
    @airline = airline
    mail(to: airline.user.email, subject: 'New airline was created!')
  end
end
