# frozen_string_literal: true

# == Schema Information
#
# Table name: airlines
#
#  id         :bigint           not null, primary key
#  country    :string
#  image_url  :string
#  name       :string
#  popularity :integer          default(0)
#  slug       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :bigint
#
# Indexes
#
#  index_airlines_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
class AirlineSerializer
  include FastJsonapi::ObjectSerializer
  attributes :name, :image_url, :slug, :avg_score, :popularity, :country, :user_id

  has_many :reviews
end
