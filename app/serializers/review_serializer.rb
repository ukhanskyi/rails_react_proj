# frozen_string_literal: true

# == Schema Information
#
# Table name: reviews
#
#  id          :bigint           not null, primary key
#  description :string
#  score       :integer
#  title       :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  airline_id  :bigint           not null
#
# Indexes
#
#  index_reviews_on_airline_id  (airline_id)
#
# Foreign Keys
#
#  fk_rails_...  (airline_id => airlines.id)
#
class ReviewSerializer
  include FastJsonapi::ObjectSerializer
  attributes :title, :description, :score
end
