# frozen_string_literal: true

# Class For Daily Exchange Rate Job
class AddDailyExchangeRateJob < ApplicationJob
  queue_as :default

  def perform
    exchange_rates = JSON.parse(ExchangeRateService.new.exchange_rates)

    exchange_rates.map do |exchange_rate|
      ExchangeRate.new(ccy: exchange_rate['ccy'], base_ccy: exchange_rate['base_ccy'],
                       buy: exchange_rate['buy'], sale: exchange_rate['sale']).save
    end
  end
end
