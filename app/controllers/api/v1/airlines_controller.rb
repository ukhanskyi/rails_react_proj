# frozen_string_literal: true

module Api
  module V1
    # class for Airline
    class AirlinesController < ApplicationController
      protect_from_forgery with: :null_session

      def index
        airlines = Airline.all.ordered

        render json: AirlineSerializer.new(airlines, options).serialized_json
      end

      def show
        airline = Airline.find_by(slug: params[:slug])

        render json: AirlineSerializer.new(airline, options).serialized_json
      end

      def create
        airline = Airline.new(airline_params)

        if airline.save
          render json: AirlineSerializer.new(airline).serialized_json
        else
          render json: { error: airline.errors.messages }, status: 422
        end
      end

      def update
        airline = Airline.find_by(slug: params[:slug])

        if airline.update(airline_params)
          render json: AirlineSerializer.new(airline, options).serialized_json
        else
          render json: { error: airline.errors.messages }, status: 422
        end
      end

      def destroy
        airline = Airline.find_by(slug: params[:slug])

        head :no_content if airline.destroy
      end

      def statistic
        airlines_statistic = Airline.calculate_number_of_airlines_by_country

        render json: airlines_statistic.to_json
      end

      private

      def airline_params
        params.require(:airline).permit(:name, :image_url, :popularity, :country, :user_id)
      end

      def options
        @options ||= { include: %i[reviews] }
      end
    end
  end
end
