# frozen_string_literal: true

module Api
  module V1
    # class for Exchange Rate
    class ExchangeRatesController < ApplicationController
      protect_from_forgery with: :null_session

      def exchange_rate
        exchange_rates = ExchangeRate.last(4)

        render json: ExchangeRateSerializer.new(exchange_rates).serialized_json
      end
    end
  end
end
