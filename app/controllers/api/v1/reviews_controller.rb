# frozen_string_literal: true

module Api
  module V1
    # Class for Reviews
    class ReviewsController < ApplicationController
      def create
        review = airline.reviews.new(review_params)

        if review.save
          render json: ReviewSerializer.new(review).serialized_json
        else
          render json: { error: review.errors.messages }, status: 422
        end
      end

      def destroy
        review = Review.find(params[:id])

        head :no_content if review.destroy
      end

      private

      def airline
        @airline ||= Airline.find(params[:airline_id])
      end

      def review_params
        params.require(:review).permit(:title, :description, :score)
      end
    end
  end
end
