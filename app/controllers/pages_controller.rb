# frozen_string_literal: true

# Class for Rages
class PagesController < ApplicationController
  def index; end
end
