# frozen_string_literal: true

# == Schema Information
#
# Table name: airlines
#
#  id         :bigint           not null, primary key
#  country    :string
#  image_url  :string
#  name       :string
#  popularity :integer          default(0)
#  slug       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :bigint
#
# Indexes
#
#  index_airlines_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#

# Class for Airline
class Airline < ApplicationRecord
  has_many :reviews

  belongs_to :user

  before_create :slugify

  mount_uploader :image_url, ImageUploader

  validates :name, presence: true, length: 3..50
  validates_format_of :name, with: /\A[^0-9`!@#$%\^&*+_=]+\z/

  scope :ordered, -> { order(popularity: :desc) }

  after_create :send_email_about_new_airline

  def send_email_about_new_airline
    AirlineMailer.send_email_about_new_airline(self).deliver_now
  end

  def slugify
    self.slug = name.parameterize
  end

  def avg_score
    return 0 unless reviews.count.positive?

    reviews.average(:score).round(2).to_f
  end

  def name_reverse
    name.reverse
  end

  def self.calculate_number_of_airlines_by_country
    airlines = Airline.group(:country).count
    airlines.map { |key, value| { country: key, count: value } }
  end
end
