# frozen_string_literal: true

# == Schema Information
#
# Table name: exchange_rates
#
#  id         :bigint           not null, primary key
#  base_ccy   :string           default(""), not null
#  buy        :decimal(10, 5)   not null
#  ccy        :string           default(""), not null
#  sale       :decimal(10, 5)   not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class ExchangeRate < ApplicationRecord
end
