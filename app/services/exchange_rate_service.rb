# frozen_string_literal: true

# Class for Exchange Rate
class ExchangeRateService
  include HTTParty

  def exchange_rates
    HTTParty.get('https://api.privatbank.ua/p24api/pubinfo?exchange&json&coursid=11').body
  end
end
