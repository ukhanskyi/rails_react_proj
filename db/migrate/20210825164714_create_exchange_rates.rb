# frozen_string_literal: true

# Class for creating Exchange Rates
class CreateExchangeRates < ActiveRecord::Migration[6.1]
  def change
    create_table :exchange_rates do |t|
      t.string  :ccy,      null: false, default: ''
      t.string  :base_ccy, null: false, default: ''
      t.decimal :buy,      null: false, precision: 10, scale: 5
      t.decimal :sale,     null: false, precision: 10, scale: 5

      t.timestamps
    end
  end
end
