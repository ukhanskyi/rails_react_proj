# frozen_string_literal: true

# Add zero as default value Class
class AddDefaultValueToColumnPopularity < ActiveRecord::Migration[6.1]
  def up
    change_column :airlines, :popularity, :integer, default: 0
  end

  def down
    change_column :airlines, :popularity, :integer
  end
end
