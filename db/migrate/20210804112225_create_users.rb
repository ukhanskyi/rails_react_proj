# frozen_string_literal: true

# Class for creating User
class CreateUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :users do |t|
      t.string :first_name, null: false, default: ''
      t.string :last_name, null: false, default: ''
      t.string :email, null: false, default: ''
      t.string :role, null: false, default: 'user'

      t.timestamps
    end
  end
end
