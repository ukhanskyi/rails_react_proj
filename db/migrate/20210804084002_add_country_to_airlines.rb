# frozen_string_literal: true

# Add fielt contry to airlines table
class AddCountryToAirlines < ActiveRecord::Migration[6.1]
  def change
    add_column :airlines, :country, :string
  end
end
