# frozen_string_literal: true

# Class for reference User to Airline
class AddUserToAirlines < ActiveRecord::Migration[6.1]
  def change
    add_reference :airlines, :user, foreign_key: true
  end
end
