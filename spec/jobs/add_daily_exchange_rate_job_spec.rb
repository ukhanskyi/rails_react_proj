# frozen_string_literal: true

require 'rails_helper'
require 'sidekiq/testing'
Sidekiq::Testing.fake!

RSpec.describe AddDailyExchangeRateJob, type: :job do
  let(:time)                      { (Time.zone.today + 3.hours).to_datetime }
  let(:scheduled_job)             { described_class.perform_later(time, 'AddDailyExchangeRateJob', true) }
  let(:expected_servise_response) do
    "[{\"ccy\":\"USD\",\"base_ccy\":\"UAH\",\"buy\":\"26.65000\",\"sale\":\"26.95418\"},
      {\"ccy\":\"EUR\",\"base_ccy\":\"UAH\",\"buy\":\"31.20000\",\"sale\":\"31.64557\"},
      {\"ccy\":\"RUR\",\"base_ccy\":\"UAH\",\"buy\":\"0.35000\",\"sale\":\"0.38000\"},
      {\"ccy\":\"BTC\",\"base_ccy\":\"USD\",\"buy\":\"44677.6719\",\"sale\":\"49380.5847\"}]"
  end

  before do
    allow_any_instance_of(ExchangeRateService).to receive(:exchange_rates).and_return(expected_servise_response)
  end

  subject { AddDailyExchangeRateJob }

  it 'job in correct queue' do
    subject.perform_now
    assert_equal 'default', subject.queue_as
  end

  it 'matches with enqueued job' do
    ActiveJob::Base.queue_adapter = :test
    expect do
      subject.perform_later
    end.to enqueue_job(subject)
  end

  describe '#perform' do
    it 'uploads a exchange_rate ' do
      ActiveJob::Base.queue_adapter = :test

      expect do
        subject.set(wait_until: Date.today.midnight, queue: 'default').perform_later('add_daily_exchange_rate')
      end.to have_enqueued_job.with('add_daily_exchange_rate').on_queue('default').at(Date.today.midnight)

      expect { subject.perform_now }.to change(ExchangeRate, :count).by 4
    end
  end
end
