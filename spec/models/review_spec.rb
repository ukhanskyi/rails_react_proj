# frozen_string_literal: true

# == Schema Information
#
# Table name: reviews
#
#  id          :bigint           not null, primary key
#  description :string
#  score       :integer
#  title       :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  airline_id  :bigint           not null
#
# Indexes
#
#  index_reviews_on_airline_id  (airline_id)
#
# Foreign Keys
#
#  fk_rails_...  (airline_id => airlines.id)
#
require 'rails_helper'

RSpec.describe Review, type: :model do
  let!(:airline) { FactoryBot.create(:airline) }
  let!(:review)  { FactoryBot.create(:review, airline_id: airline.id, score: 3) }

  describe 'factories' do
    it 'has valid factory' do
      expect(review).to be_valid
    end
  end
end
