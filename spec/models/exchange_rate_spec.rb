# frozen_string_literal: true

# == Schema Information
#
# Table name: exchange_rates
#
#  id         :bigint           not null, primary key
#  base_ccy   :string           default(""), not null
#  buy        :decimal(10, 5)   not null
#  ccy        :string           default(""), not null
#  sale       :decimal(10, 5)   not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
require 'rails_helper'

RSpec.describe ExchangeRate, type: :model do
  describe 'factories' do
    it 'has valid factory' do
      expect(FactoryBot.create(:exchange_rate)).to be_valid
    end
  end
end
