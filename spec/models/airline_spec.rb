# frozen_string_literal: true

# == Schema Information
#
# Table name: airlines
#
#  id         :bigint           not null, primary key
#  country    :string
#  image_url  :string
#  name       :string
#  popularity :integer          default(0)
#  slug       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :bigint
#
# Indexes
#
#  index_airlines_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#
require 'rails_helper'

RSpec.describe Airline, type: :model do
  let!(:airline)     { FactoryBot.create(:airline, name: 'ABC', country: 'Croatia') }
  let!(:uk_airline1) { FactoryBot.create(:airline, :ukrainians_airline) }
  let!(:uk_airline2) { FactoryBot.create(:airline, :ukrainians_airline) }
  let!(:review)      { FactoryBot.create(:review, airline_id: airline.id, score: 3) }
  let!(:reviews)     { FactoryBot.create_list(:review, 2, airline_id: airline.id, score: 3) }

  describe 'factories' do
    it 'has valid factory' do
      expect(FactoryBot.create(:airline)).to be_valid
    end
  end

  describe '#slugify' do
    it { expect(airline.slug).to eq('abc') }
  end

  describe '#calculate_average' do
    it { expect(airline.reviews.size).to eq 3 }
    it { expect(airline.reviews.sum(:score)).to eq 9 }

    it 'sets the review avg on airline' do
      expect(airline.avg_score).to eq 3.to_d
    end
  end

  describe '#name_reverse' do
    it 'reverses a name of airline' do
      allow(airline).to receive(:name) { 'ABC' }

      expect(airline.name_reverse).to eq('CBA')
    end
  end

  describe '#calculate_number_of_airlines_by_country' do
    context 'when airlines exist' do
      it 'returns count of airlines by every country' do
        expect(described_class.calculate_number_of_airlines_by_country).to eq([{ count: 2, country: 'Ukraine' },
                                                                               { count: 1, country: 'Croatia' }])
      end
    end
  end
end
