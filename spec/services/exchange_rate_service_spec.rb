# frozen_string_literal: true

# Class for Exchange Rate
require 'spec_helper'
require 'rails_helper'

RSpec.describe ExchangeRateService, type: :service do
  subject(:api) { described_class.new }

  describe '#exchange_rates' do
    let(:privat_bank_url)           { 'https://api.privatbank.ua/p24api/pubinfo?exchange&json&coursid=11' }
    let(:privat_bank_response)      { instance_double(HTTParty::Response, body: privat_bank_response_body) }
    let(:privat_bank_response_body) do
      "[{\"ccy\":\"USD\",\"base_ccy\":\"UAH\",\"buy\":\"26.65000\",\"sale\":\"26.95418\"},
        {\"ccy\":\"EUR\",\"base_ccy\":\"UAH\",\"buy\":\"31.20000\",\"sale\":\"31.64557\"},
        {\"ccy\":\"RUR\",\"base_ccy\":\"UAH\",\"buy\":\"0.35000\",\"sale\":\"0.38000\"},
        {\"ccy\":\"BTC\",\"base_ccy\":\"USD\",\"buy\":\"44677.6719\",\"sale\":\"49380.5847\"}]"
    end

    before do
      allow(HTTParty).to receive(:get).and_return(privat_bank_response)

      api.exchange_rates
    end

    it 'fetches the exchange_rates from PrivatBank api' do
      expect(HTTParty).to have_received(:get).with(privat_bank_url)
    end
  end
end
