# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::ExchangeRatesController, type: :controller do
  describe 'GET #exchange_rate' do
    before { get :exchange_rate, format: :json }

    it { is_expected.to respond_with :success }
  end
end
