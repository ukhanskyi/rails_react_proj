# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::ReviewsController, type: :controller do
  let!(:user)           { FactoryBot.create(:user) }

  let!(:airline)        { FactoryBot.create(:airline, user_id: user.id) }
  let!(:review)         { FactoryBot.create(:review, airline_id: airline.id) }

  let!(:valid_params)   { FactoryBot.attributes_for(:review) }
  let!(:invalid_params) { FactoryBot.attributes_for(:review, title: nil, description: nil, score: nil) }

  before { user }

  describe 'Reviews #create' do
    context 'when creates review with valid parameters' do
      subject { post :create, params: { airline_id: airline.id, review: valid_params } }

      it 'creates a new review' do
        expect { subject }.to change(Review, :count).by(+1)
      end

      it { is_expected.to have_http_status(:success) }
    end

    context 'with invalid parameters' do
      subject { post :create, params: { airline_id: airline.id, review: invalid_params } }

      it 'doesn`t creates a new review' do
        expect { subject }.to change(Review, :count).by(0)
      end
    end
  end

  describe 'DELETE#destroy' do
    it 'destroys the review' do
      expect { delete :destroy, params: { id: review.id } }
        .to change(Review, :count).by(-1)
    end
  end
end
