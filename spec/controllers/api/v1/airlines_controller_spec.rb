# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::AirlinesController, type: :controller do
  let!(:user)           { FactoryBot.create(:user) }
  let!(:airline)        { FactoryBot.create(:airline, user_id: user.id) }
  let!(:valid_params)   { FactoryBot.attributes_for :airline, user_id: user.id }
  let!(:invalid_params) { FactoryBot.attributes_for(:airline, name: '', image_url: '') }

  before { user }

  describe 'GET #index' do
    before { get :index, format: :json }

    it { is_expected.to respond_with :success }
  end

  describe 'GET #statistic' do
    before { get :statistic, format: :json }

    it { is_expected.to respond_with :success }
  end

  describe 'GET #show' do
    before { get :show, format: :json, params: { slug: airline.slug } }

    it { is_expected.to respond_with :success }
  end

  describe 'Airline #create' do
    context 'when creates airline with valid parameters' do
      subject { post :create, format: :json, params: { airline: valid_params } }

      it 'creates an airline' do
        expect { subject }.to change(Airline, :count).by(+1)
      end

      it { is_expected.to have_http_status(:success) }
    end

    context 'with invalid parameters' do
      subject { post :create, format: :json, params: { airline: invalid_params } }

      it 'doesn`t creates a new airline' do
        expect { subject }.to change(Airline, :count).by(0)
      end
    end
  end

  describe 'PUT#update' do
    context 'with valid params' do
      before do
        put :update, format: :json, params: {
          slug: airline.slug, airline: valid_params.merge!(
            name: 'Example',
            image_url: 'https://images.twinkl.co.uk/tw1n/image/private/t_630_eco/website/uploaded/img-4-1611649448.jpg',
            popularity: 8
          )
        }
      end

      it { is_expected.to respond_with :success }
    end

    context 'with invalid params' do
      before do
        put :update, format: :json, params: {
          slug: airline.slug, airline: valid_params.merge!(
            name: '',
            image_url: '',
            popularity: 0
          )
        }
      end

      it { is_expected.to respond_with :unprocessable_entity }
    end
  end

  describe 'DELETE#destroy' do
    it 'destroys the airline' do
      expect { delete :destroy, params: { slug: airline.slug } }
        .to change(Airline, :count).by(-1)
    end
  end
end
