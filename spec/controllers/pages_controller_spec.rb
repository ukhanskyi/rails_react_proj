# frozen_string_literal: true

require 'rails_helper'

RSpec.describe PagesController, type: :controller do
  render_views

  it 'opens GET #index' do
    get :index
    expect(response).to be_successful
  end
end
