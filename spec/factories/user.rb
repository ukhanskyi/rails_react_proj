# frozen_string_literal: true

# Class for User
FactoryBot.define do
  factory :user do
    first_name { Faker::Name.first_name }
    last_name  { Faker::Name.last_name }
    email      { Faker::Internet.email }
    role       { %w[user admin].sample }
  end

  trait :admin do
    role { 'admin' }
  end
end
