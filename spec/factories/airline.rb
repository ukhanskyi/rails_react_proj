# frozen_string_literal: true

# Class for Airline
FactoryBot.define do
  factory :airline do
    name       { Faker::Company.name }
    image_url  { Rack::Test::UploadedFile.new(Rails.root + "spec/files/images/#{rand(1..10)}.png", 'image/png') }
    popularity { Faker::Number.between(from: 0, to: 10) }
    slug       { name.parameterize }
    country    { Faker::Address.country }
    association :user, factory: :user
  end

  trait :ukrainians_airline do
    country { 'Ukraine' }
  end
end
