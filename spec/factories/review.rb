# frozen_string_literal: true

# Class for Review
FactoryBot.define do
  factory :review do
    title       { Faker::Lorem.sentence }
    description { Faker::Lorem.paragraph(sentence_count: 2..5) }
    score       { Faker::Number.between(from: 1, to: 5) }
    association :airline, factory: :airline
  end
end
