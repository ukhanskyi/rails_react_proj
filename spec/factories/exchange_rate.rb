# frozen_string_literal: true

# Class for ExchangeRate
FactoryBot.define do
  factory :exchange_rate do
    base_ccy { Faker::Currency.code }
    ccy      { Faker::Currency.code }
    buy      { Faker::Commerce.price(range: 0.01..1000.0) }
    sale     { Faker::Commerce.price(range: buy..1000.0) }
  end
end
