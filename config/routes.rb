# frozen_string_literal: true

require 'sidekiq/web'
require 'sidekiq-cron'

Rails.application.routes.draw do
  root 'pages#index'

  mount Sidekiq::Web => '/sidekiq'

  namespace :api do
    namespace :v1 do
      resources :airlines, param: :slug
      resources :reviews, only: %i[create destroy]
      get 'statistic', to: 'airlines#statistic'
      get 'exchange_rate', to: 'exchange_rates#exchange_rate'
    end
  end

  get '*path', to: 'pages#index', via: :all
end
