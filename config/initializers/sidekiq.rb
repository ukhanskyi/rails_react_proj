# frozen_string_literal: true

Sidekiq.configure_server do |config|
  Redis.exists_returns_integer = false
  config.redis = { url: 'redis://localhost:6379/' }
  schedule_file = 'config/schedule.yml'
  Sidekiq::Cron::Job.load_from_hash YAML.load_file(schedule_file) if File.exists?(schedule_file)
  sleep 2
end
